import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Linha } from '../model/linha';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Itinerario } from '../model/itinerario';


@Injectable({
  providedIn: 'root'
})
export class ApiLinhaService {

  private apiOnibusUrl : string =  "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o"

  private apiLotacaoUrl : string =  "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l"

  private apiItinerarioUrl: string = "http://www.poatransporte.com.br/php/facades/process.php?a=il&p="

  constructor(private http: HttpClient) { }

  getLinhasOnibus(): Observable<Linha[]>{
    return this.http.get<Linha[]>(`${this.apiOnibusUrl}`).pipe(tap
      (linhasOnibus => console.log('linhas de onibus carregadas'),catchError(this.handleError('getLinhasOnibus',[]))));
  }

  getLinhasLotacao(): Observable<Linha[]>{
    return this.http.get<Linha[]>(`${this.apiLotacaoUrl}`).pipe(tap
      (linhaLotacao => console.log('linhas de lotacao carregadas'),catchError(this.handleError('getLinhasLotacao',[]))));
  }

  getItinerarioPorId(id: number): Observable<Itinerario>{
    return this.http.get<Itinerario>(`${this.apiItinerarioUrl}+${id}`).pipe(tap
      (itinerario => console.log('itinerario carregado'),catchError(this.handleError('getItinerarioPorId',[]))));
  }

  getCoordenadas(id: number): Promise<number>{
    return this.http.get(`${this.apiItinerarioUrl}+${id}`).toPromise()
    .then((retorno: any) => {
      return retorno;
    })
  }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
