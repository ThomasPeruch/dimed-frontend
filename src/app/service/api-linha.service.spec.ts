import { TestBed } from '@angular/core/testing';

import { ApiLinhaService } from './api-linha.service';

describe('ApiLinhasOnibusService', () => {
  let service: ApiLinhaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiLinhaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
