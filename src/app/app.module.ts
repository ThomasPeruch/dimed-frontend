import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing-module';
import { AppComponent } from './app.component';
import { LinhaOnibusComponent } from './linha-onibus/linha-onibus.component';
import { LinhaLotacaoComponent } from './linha-lotacao/linha-lotacao.component';
import { HttpClientModule } from '@angular/common/http';
import { ListaLinhasOnibusComponent } from './linha-onibus/lista-linhas-onibus/lista-linhas-onibus.component';
import { ItinerarioComponent } from './itinerario/itinerario.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './template_components/header/header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FooterComponent } from './template_components/footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    AppComponent,
    LinhaOnibusComponent,
    LinhaLotacaoComponent,
    ListaLinhasOnibusComponent,
    ItinerarioComponent,
    HeaderComponent,
    FooterComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FontAwesomeModule,
    FormsModule,
    Ng2SearchPipeModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  
 }
