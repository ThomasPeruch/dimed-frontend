import { Component, OnInit } from '@angular/core';
import { ApiLinhaService } from '../service/api-linha.service';
import { Linha } from '../model/linha';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-linha-onibus',
  templateUrl: './linha-onibus.component.html',
  styleUrls: ['./linha-onibus.component.css']
})
export class LinhaOnibusComponent implements OnInit {

  linhas: Linha[] = [];
  pesquisaLinha: string='';
  linhaEscolhidaId: number;
  faSearch = faSearch;
  loader_spinner= true;

  constructor(private service: ApiLinhaService, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.service.getLinhasOnibus().subscribe(linha => {
      this.linhas = linha;
      console.log(this.linhas);
      this.loader_spinner=false
    });
  }

  redirecionaItinerario(id: number){  
    this.router.navigateByUrl("/itinerario/"+id);
  }

}
