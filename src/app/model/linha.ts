import { Coordenadas } from "./coordenadas";

export interface Linha{
    id: number;
    codigo: string;
    nome: string;
}