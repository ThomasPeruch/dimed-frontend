import { Coordenadas } from "./coordenadas";

export interface Itinerario {
    idlinha: number;
    nome: string;
    codigo: string;

    rota: Coordenadas[];
}