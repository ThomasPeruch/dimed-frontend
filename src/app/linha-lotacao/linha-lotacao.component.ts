import { Component, OnInit } from '@angular/core';
import { Linha } from '../model/linha';
import { ApiLinhaService } from '../service/api-linha.service';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';


@Component({
  selector: 'app-linha-lotacao',
  templateUrl: './linha-lotacao.component.html',
  styleUrls: ['./linha-lotacao.component.css']
})
export class LinhaLotacaoComponent implements OnInit {

  faSearch = faSearch;
  pesquisaLinha: string='';
  idLinha:number=0;
  linhas: Linha[] = []; 
  loader_spinner= true;

  constructor(private service: ApiLinhaService, private router: Router) { }

  ngOnInit(): void {
    this.service.getLinhasLotacao().subscribe(linha => {
      this.linhas = linha;
      console.log(this.linhas);
      this.loader_spinner = false;
    });
  }

  redirecionaItinerario(id: number){  
    this.router.navigateByUrl("/itinerario/"+id);
  }
}
