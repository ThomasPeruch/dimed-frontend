import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LinhaOnibusComponent } from './linha-onibus/linha-onibus.component';
import { LinhaLotacaoComponent } from './linha-lotacao/linha-lotacao.component';
import { ItinerarioComponent } from './itinerario/itinerario.component';

const routes: Routes = [
    {
        path: 'linhas-onibus',
        component: LinhaOnibusComponent,
        data: { title : 'Linhas de Onibus'}
    },
    {
        path: 'linhas-lotacao',
        component: LinhaLotacaoComponent,
        data: { title : 'Linhas de Lotacao'}
    },
    {
        path: 'itinerario',
        children:[
            {
                path: ':id',
                component: ItinerarioComponent,
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule{}