import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Coordenadas } from '../model/coordenadas';
import { Itinerario } from '../model/itinerario';
import { ApiLinhaService } from '../service/api-linha.service';
import { faMapMarkedAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-itinerario',
  templateUrl: './itinerario.component.html',
  styleUrls: ['./itinerario.component.css']
})
export class ItinerarioComponent implements OnInit {

  faMapMarkedAlt = faMapMarkedAlt;
  itinerario = {} as Itinerario;
  coords = {} as Coordenadas
  coord = Array<Coordenadas>();
  cont: any
  id: number;
  googleUrl: string= "www.google.com/maps/?q=";
  loader_spinner = true;

  constructor(private service: ApiLinhaService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;  
    this.service.getItinerarioPorId(this.id).subscribe((itinerario: Itinerario) => {
      this.itinerario = itinerario;
      this.itinerario.idlinha = itinerario.idlinha;
      this.itinerario.codigo = itinerario.codigo;
      this.itinerario.nome = itinerario.nome;
    }); 
    
   
    this.service.getCoordenadas(this.id).then((retorno:any) => {
      for(let i = 0 ; i < Object.keys(retorno).length -3 ; i++){
        this.coord.push(retorno[i])
      }
      this.itinerario.rota = this.coord;
      this.loader_spinner = false;
    })

  
  }

  redirecionaGoogle(lat:number, lng: number){
    window.open(this.googleUrl+lat+","+lng);
  }

}

